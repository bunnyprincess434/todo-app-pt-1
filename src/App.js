import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };

  handleText = (event) => {
    this.setState ({value: event.target.value})
  }

  handleSubmit = (event) => {
    if (event.key === 'Enter') {
      this.handleTodo();
    }
  }

  handleTodo = () => {
    const Todo = {
      "UserId" : 1,
      "id" : Math.floor(Math.random() * 9999999),
      "title" : this.state.value,
      "completed" : false,
    }

    const added = [...this.state.todos, Todo]
    this.setState ({
      todos: added,
      value: '',
    })
  }

  handleCheck = (TodoId) => {
    const check = this.state.todos.map(
      (item) => {
        if(item.id === TodoId) {
          return {...item, completed: !item.completed}
        }
        return {...item}
      }
    )
    this.setState({todos: check})
  }

  handleDelete = TodoId => {
    const newTodos = this.state.todos.filter(
      todo => todo.id !== TodoId
    )
    this.setState({todos: newTodos})
  }

  handleClear = TodoId => {
    const newTodos = this.state.todos.filter(
      todo => todo.completed !== true
    )
    this.setState({todos: newTodos})
  }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          className="new-todo" 
            placeholder="What needs to be done?" 
            autofocus 
            type="text"
            onChange={this.handleText}
            onKeyDown={this.handleSubmit}
            value={this.state.value}
          />
        </header>
        <TodoList 
          todos={this.state.todos} 
          handleCheck={this.handleCheck}
          handleDelete={this.handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.handleClear} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
            className="toggle" 
            type="checkbox" 
            checked={this.props.completed} 
            onChange={(event) => this.props.handleCheck(this.props.id)}
          />
          <label>{this.props.title}</label>
          <button 
            onClick={(event) => this.props.handleDelete(this.propes.id)}
            className="destroy" 
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
              title={todo.title} 
              completed={todo.completed} 
              handleCheck={this.props.handleCheck}
              id={todo.id}
              handleDelete={this.props.handleDelete}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
